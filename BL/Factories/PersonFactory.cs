﻿using BL.IFactories;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL.Factories
{
    public class PersonFactory : IPersonFactory
    {
        private readonly IVehicleFactory _vehicleFactory;

        public PersonFactory(IVehicleFactory vehicleFactory)
        {
            _vehicleFactory = vehicleFactory;
        }

        public PersonDTO Transform(Person p)
        {
            return new PersonDTO
            {
                PersonId = p.PersonId,
                ClosingTime = p.ClosingTime,
                Created = p.Created,
                Title = p.Title,
                Description = p.Description,
                Status = p.Status,
                IsVisible = p.IsVisible
            };
        }

        public Person Transform(PersonDTO dto)
        {
            return new Person()
            {
                PersonId = dto.PersonId,
                ClosingTime = dto.ClosingTime,
                Created = dto.Created,
                Title = dto.Title,
                Description = dto.Description,
                Status = dto.Status,
                IsVisible = dto.IsVisible

            };
        }

        public PersonDTO TransformWithObjects(Person p)
        {
            var dto = Transform(p);
            if (dto == null) return null;

            dto.Vehicles = p?.Vehicles
                .Select(c => _vehicleFactory.Transform(c)).ToList();
            return dto;
        }
    }
}
