﻿using BL.IFactories;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Factories
{
    public class VehicleFactory : IVehicleFactory
    {
        public VehicleDTO Transform(Vehicle v)
        {
            return new VehicleDTO
            {
                VehicleId = v.VehicleId,
                Deadline = v.Deadline,
                Priority = v.Priority,
                PersonId = v.PersonId,
                Created = v.Created,
                Title = v.Title,
                Description = v.Description,
                Status = v.Status,
                IsVisible = v.IsVisible

            };
        }

        public Vehicle Transform(VehicleDTO dto)
        {
            return new Vehicle
            {
                Deadline = dto.Deadline,
                Priority = dto.Priority,
                PersonId = dto.PersonId,
                Created = dto.Created,
                Title = dto.Title,
                Description = dto.Description,
                Status = dto.Status,
                IsVisible = dto.IsVisible

            };
        }
    }
}
