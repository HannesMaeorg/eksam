﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL
{
    public interface IPersonService
    {
        List<PersonDTO> GetAllPersons();
        PersonDTO AddNewPerson(PersonDTO newPerson);

        //PersonDTO GetPersonById(int personId);
        //PersonDTO FindByPersonalCode(String code);
        //List<PersonDTO> FindByName(String name);

        //PersonDTO UpdatePerson(int id, PersonDTO dto);

        //PersonDTO MarkHidden(int id);
        //bool DeletePerson(int id);
    }
}
