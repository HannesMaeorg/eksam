﻿using Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace BL
{
    public class PersonDTO : BaseDomain
    {
        public int PersonId { get; set; }
        public DateTime ClosingTime { get; set; }
        public virtual List<VehicleDTO> Vehicles { get; set; } = new List<VehicleDTO>();
        
        public static PersonDTO CreateFromDomain(Person p)
        {
            if (p == null) return null;

            return new PersonDTO()
            {
                PersonId = p.PersonId,
                ClosingTime = p.ClosingTime,
                Created = p.Created,
                Title = p.Title,
                Description = p.Description,
                Status = p.Status,
                IsVisible = p.IsVisible

            };
        }

        public static PersonDTO CreateFromDomainWithContacts(Person p)
        {
            var person = CreateFromDomain(p);
            if (person == null) return null;

            person.Vehicles = p?.Vehicles?
                .Select(c => VehicleDTO.CreateFromDomain(c)).ToList();
            return person;
          
        }
    }
}