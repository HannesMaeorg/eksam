﻿using Domain;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BL
{
    public class VehicleDTO : BaseDomain
    {
        public int VehicleId { get; set; }
        public DateTime Deadline { get; set; }
        public Priority Priority { get; set; }

        public int PersonId { get; set; }

        public static VehicleDTO CreateFromDomain(Vehicle ct)
        {
            return new VehicleDTO()
            {
                VehicleId = ct.VehicleId,
                Deadline = ct.Deadline,
                Priority = ct.Priority,
                Title = ct.Title,
                Description = ct.Description,
                Status = ct.Status,
                Created = ct.Created,
                IsVisible = ct.IsVisible
            };
        }
    }
}






