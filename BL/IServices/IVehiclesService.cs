﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BL.IServices
{
    public interface IVehiclesService
    {
        List<VehicleDTO> GetAll();
        VehicleDTO AddVehicle(VehicleDTO dto);

        //List<VehicleDTO> GetByPersonId(int id);

        //Task<VehicleDTO> GetById(int id);

        //VehicleDTO FindByNumberPlate(String numb);

        //VehicleDTO UpdateVehicle(int id, VehicleDTO dto);

        //bool DeleteVehicle(int id);

    }
}
