﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL;
using DAL.App.Interfaces;
using Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers.api
{
   
    [Route("api/People")]
    public class PeopleController : Controller
    {
        private readonly IPersonService _personService;

        public PeopleController(IPersonService personService)
        {
            _personService = personService;
        }


        [HttpGet]
        [Route("")]
        public List<PersonDTO> GetPeople() => _personService.GetAllPersons();


        //[HttpGet("{id:int}")]
        //public IActionResult GetPersonById(int id)
        //{
        //    var person = _personService.GetPersonById(id);
        //    if (person == null) return NotFound("No person with id " + id + " found");
        //    return Ok(person);
        //}

        //[HttpGet]
        //[Route("GetByPersonalCode")]
        //public IActionResult FindByPersonalCode(string code)
        //{
        //    var person = _personService.FindByPersonalCode(code);
        //    if (person == null) return BadRequest("No person with personal code " + code + " found");
        //    return Ok(person);
        //}

        //[HttpGet]
        //[Route("FindByName")]
        //public IActionResult FindByName(string name)
        //{
        //    var person = _personService.FindByName(name);
        //    if (person == null) return BadRequest("No person with name " + name + " found");
        //    return Ok(person);
        //}

        [HttpPost]
        [Route("AddPerson")]
        public IActionResult AddPerson([FromBody]PersonDTO p)
        {
            if (!ModelState.IsValid) return BadRequest("Model state invalid");

            var person = _personService.AddNewPerson(p);
            if (person == null) return BadRequest("Person is already in database");
            return Ok("Task " + person.Title + " added");
        }



        //[HttpPut("{id:int}", Name = "UpdatePerson")]
        //public IActionResult UpdatePerson(int id, [FromBody]PersonDTO dto)
        //{
        //    if (!ModelState.IsValid) return BadRequest("Model state invalid");
        //    var person = _personService.UpdatePerson(id, dto);
        //    if (person == null) return BadRequest("Person with id " + id + " not found");
        //    return Ok("Modified person " + person.Firstname + " " + person.Lastname + " with personal code " + person.PersonalCode);
        //}

        //[HttpPut]
        //[Route("MarkHidden")]
        //public IActionResult MarkHidden(int id)
        //{
        //    var person = _personService.MarkHidden(id);
        //    if (person == null) return BadRequest("Person with id " + id + " not found");
        //    return Ok("Person " + person.Firstname + " " + person.Lastname + " with personal code " + person.PersonalCode + " is now hidden");
        //}

        //[HttpDelete("{id:int}", Name = "DeletePerson")]
        //public IActionResult DeletePerson(int id)
        //{
        //    bool person = _personService.DeletePerson(id);
        //    if (person == false) return BadRequest("Person with id " + id + " not found");
        //    return Ok("Person with id " + id + " deleted!");

        //}


    }
}