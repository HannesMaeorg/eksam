﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL;
using BL.IServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers.api
{
    [Produces("application/json")]
    [Route("api/Vehicles")]
    public class VehiclesController : Controller
    {

        private readonly IVehiclesService _vehiclesService;

        public VehiclesController(IVehiclesService vehiclesService)
        {
            _vehiclesService = vehiclesService;
        }

        // GET: api/Vehicles
        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetVehicles()
        {
            var vehicles = _vehiclesService.GetAll();
            return Ok(vehicles);

        }

        //// GET: api/Vehicles/5
        //[HttpGet("{id}", Name = "GetById")]
        //public async Task<IActionResult> GetById(int id)
        //{
        //    var vehicle = await _vehiclesService.GetById(id);
        //    if (vehicle == null) return BadRequest("Vehicle with id " + id + " not found");
        //    return Ok(vehicle);
        //}

        //[HttpGet]
        //[Route("GetByPersonId")]
        //public async Task<IActionResult> GetByPersonId(int id)
        //{
        //    var vehicles = _vehiclesService.GetByPersonId(id);
        //    if (vehicles == null) return BadRequest("No vehicles with PersonId " + id + " found");
        //    return Ok(vehicles);

        //}

        //[HttpGet]
        //[Route("GetByNumberPlate")]
        //public async Task<IActionResult> GetByNumberPlate(String numb)
        //{
        //    var vehicle = _vehiclesService.FindByNumberPlate(numb);
        //    if (vehicle == null) return BadRequest("Not found");
        //    return Ok(vehicle);
        //}

        // POST: api/Vehicles
        [HttpPost]
        [Route("AddVehicle")]
        public IActionResult AddVehicle([FromBody]VehicleDTO dto)
        {
            if (!ModelState.IsValid) return BadRequest("Model state invalid");

            var vehicle = _vehiclesService.AddVehicle(dto);
            if (vehicle == null) return BadRequest("Vehicle is already in database");
            return Ok("Added Todo: " + vehicle.Title);
        }

        //// PUT: api/Vehicles/5
        //[HttpPut("{id}", Name = "UpdateVehicle")]
        //public IActionResult UpdateVehicle(int id, [FromBody]VehicleDTO dto)
        //{
        //    if (!ModelState.IsValid) return BadRequest("Model state invalid");

        //    var vehicle =  _vehiclesService.UpdateVehicle(id, dto);
        //    if (vehicle == null) return BadRequest("Vehicle with id " + id + " not found");
        //    return Ok("Modified vehicle: " + vehicle.NumberPlate);
        //}

        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}", Name = "DeleteVehicle")]
        //public IActionResult DeleteVehicle(int id)
        //{
        //    bool result = _vehiclesService.DeleteVehicle(id);
        //    if (result == false) return BadRequest("Vehicle with id " + id + " not found");
        //    return Ok("Vehicle with id " + id + " deleted!");
        //}
    }
}
