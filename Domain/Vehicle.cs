﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
       public class Vehicle : BaseDomain
    {
        public int VehicleId { get; set; }
        public DateTime Deadline { get; set; }
        public Priority Priority { get; set; }
        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
    }
}
