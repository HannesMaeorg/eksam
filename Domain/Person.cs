﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Person : BaseDomain
    {
        public int PersonId { get; set; }
        public DateTime ClosingTime { get; set; }
        public virtual List<Vehicle> Vehicles { get; set; } = new List<Vehicle>();
    }
}
