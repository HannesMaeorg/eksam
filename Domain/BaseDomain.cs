﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class BaseDomain
    {
        public DateTime Created { get; set; }
        [MaxLength(64)]
        public string Title { get; set; }
        [MaxLength(128)]
        public string Description { get; set; }
        public Status Status { get; set; }
        public bool IsVisible { get; set; } = true;
    }
}
